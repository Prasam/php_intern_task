<?php

namespace Classes;

class User extends DB
{
    private $firstName;
    private $lastName;
    private $email;
    private $ID;

    /**
     * @return mixed
     */


    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }


    /**
     * @return mixed
     */


    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function insert()
    {
        $query = "insert into users(email,first_name, last_name) 
            values(
            '".$this->getEmail()."',
            '".$this->getFirstName()."',
            '".$this->getLastName()."'
            )";

        return mysqli_query($this->db, $query);
    }

    public function update()
    {
        $query = "update users 
                    set first_name= '".$this->getFirstName()."',
                    last_name= '".$this->getLastName()."',
                    email= '".$this->getEmail()."'
                    where id='".$this->getID()."'
                  ";

        return mysqli_query($this->db, $query);
    }
    public function delete()
    {
        $id=$this->getID();
        if (!empty ($id)) {
            $query = "delete from users where id= $id";

            return mysqli_query($this->db, $query);
        }
        return false;
    }

    public function getAll()
    {
        $query = 'select * from users';
        $result = mysqli_query($this->db, $query);
        $users = [];
        if (mysqli_num_rows($result) > 0) {
            while ($user = mysqli_fetch_assoc($result)) {
                $thisUser = new self();
                $thisUser->setID($user['id']);
                $thisUser->setFirstName($user['first_name']);
                $thisUser->setLastName($user['last_name']);
                $thisUser->setEmail($user['email']);
                $users[] = $thisUser;
            }
        }
        return $users;
    }

    public function getAllByID()
    {

        $id=$this->getID();

        $query = 'select * from users where id='.$id;
        $result = mysqli_query($this->db, $query);
            if (mysqli_num_rows($result) > 0) {
            while ($user = mysqli_fetch_assoc($result)) {
                $this->setID($user['id']);
                $this->setFirstName($user['first_name']);
                $this->setLastName($user['last_name']);
                $this->setEmail($user['email']);
            }
        }
    }
}
