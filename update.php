<?php

include 'vendor/autoload.php';
$Id = $_POST['ID'];
$email = $_POST['email'];
$firstName = $_POST['first_name'];
$lastName = $_POST['last_name'];

$user = new \Classes\User();
$user->setID($Id);
$user->setFirstName($firstName);
$user->setEmail($email);
$user->setLastName($lastName);
if ($user->update()) {
    header('location:list.php');
}
